---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
---
## Subtitulo

Breve introducción de la partida

<!--more-->

- **Sistema**: Nombre del sistema
- **Tipo partida**: Escrita, improvisada, colaborativa
- **Sesiones**: One-shot, mini-campaña, campaña
- **Jugadores**: X-Y
- **Link**: Link-al-sistema
- **Etiquetas**: las que quieras!

Parrafo descriptivo de la partida



