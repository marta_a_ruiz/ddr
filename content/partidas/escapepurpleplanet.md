---
title: "Huída del planeta púrpura"
date: 2023-02-19T20:30:13+01:00
draft: false
---
## ¿Cómo habéis llegado aquí?

Despertáis en la oscuridad. Un gran peso metálico pende de vuestras cabezas, muñecas y tobillos. Vuestras fosas nasales estallan con el hedor del metal oxidado y el sudor frío. Sonidos ahogados de violencia (gritos de dolor y aullidos de un público bestial) llegan procedentes de más arriba.

<!--more-->

- **Sistema**: Clásicos del Mazmorreo
- **Tipo partida**: Escrita
- **Sesiones**: One-shot
- **Jugadores**: 3-5
- **Link**: [Aquí](http://www.other-selves.com/p/clasicos-del-mazmorreo.html)
- **Etiquetas**: fantasía, ciencia-ficción, mazmorreo, exploración

![El Planeta Púrpura](/images/peligroplanetapurpura.png)

Sólo hace unas horas despertábais en vuestra habitación, en vuestra casa, en vuestro.... PLANETA.

Ahora estáis en el Planeta Púrpura. ¿Conseguiréis escapar de vuestros captores? Aún más difícil, ¿conseguiréis regresar a casa?

Esta aventura es un embudo de Clásicos del Mazmorreo situado en la recientemente publicada ambientación "Peligro en el Planeta Púrpura", y sirve como introducción a este mundo alienígena y letal.

Si te apetece una buena dosis de mazmorreo alienígena, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)

