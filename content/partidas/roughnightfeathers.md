---
title: "Una Noche Agitada en las Tres Plumas"
date: 2023-02-19T23:43:34+01:00
draft: false
---
## En una noche de taberna cualquiera...

En el Viejo Mundo, en el Imperio... no hay muchos héroes. Vosotros no lo sois, eso seguro, y mucho menos en una noche de juerga en la taberna!

<!--more-->

- **Sistema**: Warlock! (Warhammer Fantasy RPG)
- **Tipo partida**: Pre-Escrita
- **Sesiones**: One-shot
- **Jugadores**: 2-4
- **Link**: [Aquí](https://devir.es/devirpedia/warhammer-fantasy-el-juego-de-rol)
- **Etiquetas**: warhammer, fantasy, multi-trama

![Pelea!](/images/rough_nights.jpg)

Partida en el mundo de Warhammer, pero no esperéis ver ejércitos de Altos Elfos y Hombres Bestia batallando por controlar una fortaleza.
Aquí jugamos en los pueblos, en las ciudades, en los ríos. Entre campesinos, mercaderes, guardias, nobles... y herejes, sectarios, mutantes, ladrones y asesinos.
Los personajes no son héroes, o no necesariamente, sino gente normal con un trabajo normal y ganas de aventuras, o simplemente de salir de su vida de mierda.
Una Noche Agitada... es una de las aventuras más clásicas y reverenciadas de Warhammer Fantasy RPG ,llena de intriga, humor negro y... 
Se usará el sistema de Warlock! para reducir la carga de reglas.

Si te apetece jugar una comedia de enredo, con personajes fuera de lo heroico habitual, en un mundo fantástico pero miserable, 
apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)




