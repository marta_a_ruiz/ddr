---
title: "La Batalla de Tora Armis"
date: 2023-02-19T20:28:23+01:00
draft: false
---
## En la desalentadora oscuridad de un futuro lejano...

La batalla de Tora Armis es una mini-campaña en el mundo de Warhammer 40k. En ella, traidores herejes han tomado el mundo colmena de Tora Armis. 
A lo largo de 4 partidas, los jugadores tomarán el rol de unidades militares del Imperio en diferentes escenarios y situaciones.

<!--more-->

- **Sistema**: Wrath and Glory
- **Tipo partida**: Pre-Escrita
- **Sesiones**: Mini-campaña (6-8 sesiones)
- **Jugadores**: 2-4
- **Link**: [Aquí](https://cubicle7games.com/our-games/wrath-glory)
- **Etiquetas**: W40k, sajarraja, ciencia-ficción

![Luchando a las puertas de Tora Armis](/images/bloody-gates.jpeg)

La primera partida, Puertas Sangrientas, llevará a una brigada penal a intentar asaltar las puertas de la ciudad, después de escalar una presa, atravesar
un kilómetro de campo de minas y deshacerse de las hordas de heréticos enfermos.

En la segunda, A Lomos de Valquirias, un grupo de soldados de asalto se infiltrará en el corazón de la ciudad colmena, y con el único apoyo de pandilleros locales
de dudosa lealtad, intentará detener a Raurok, el Heraldo de los Poderes Ruinosos.

Seguimos en El Señor de la Espira, en la que los agentes al servicio del Inquisidor Tytrona Dikaisune liderarán un equipo de élite en su ataque al palacio del Señor Von Staten, 
siervo de Nurgle desde cuya residencia se extiende la pestilencia, corrupción y herejía.

Por último, en Aflicción en Ascenso, los jugadores tomarán el papel de Marines Espaciales del capítulo de los Absolvedores, encargados de limpiar la corrupción del Señor del Caos Putradyne Corporis. ¿Serás digno de la armadura de Exterminador?

Esta mini campaña se centra en la acción, infiltración y en molarse mucho en la desalentadora oscuridad de un futuro lejano en el que SOLO HAY GUERRA. 
Si esto te suena bien, elígela en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)



