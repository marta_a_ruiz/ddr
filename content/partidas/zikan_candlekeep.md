---
title: "Zikan_candlekeep"
date: 2023-02-19T20:29:57+01:00
draft: true
---
## Titulo partida (o sistema si no hay partida)

Breve introducción de la partida

- **Sistema**: Nombre del sistema
- **Tipo partida**: Escrita, improvisada, colaborativa
- **Sesiones**: One-shot, mini-campaña, campaña
- **Jugadores**: X-Y
- **Link**: Link-al-sistema
- **Etiquetas**: las que quieras!

Parrafo descriptivo de la partida



